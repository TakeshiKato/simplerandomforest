#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

using namespace std;

#define DATA_NUM 30
#define FEATURES 2
#define SUB_SET 5
#define SUB_SET_SAMPLE 10
#define CLASS 2
#define POS_CLASS 0
#define NEG_CLASS 1
#define TREE_DEPTH 4

const int NodeListNum = pow(2,TREE_DEPTH) - 1;

struct Data
{
	float x;
	float y;
	int label;
};

struct Node
{
	float threshold;
	int FeatureInd;
	int DataIndList[DATA_NUM];
	bool isLeafFlag;
	int LeftChildNode;
	int RightChildNode;
	int DataIndListSize;
	float PosProb;
	float NegProb;
};

class Tree
{
	public:
		Node NodeList[NodeListNum]; //node in tree
		void decideNodeThreshold(Node* node, Data* TrainData);
		float getGain(float threshold, Data* TrainData, int Index, int* DataIndList, int DataIndListSize);
		float getEntropy(int PosCount, int NegCount);
		void separate(Node* NodeList, Data* TrainData, int Depth, int Width, int TargetInd);
		void calcChildNodeIndex(int iDepth, int iWidth, Node* node, int TargetInd);
		bool isLeafNode(int* DataIndList, Data* TrainData, int DataNum);
		void getProb(Node* _node, Data* TrainData);
		int predict(Node* NodeList, Data TestData);
};

float Tree::getGain(float threshold, Data* TrainData, int Index, int* DataIndList, int DataIndListSize)
{
	float Gain = 0.0f;
	int leftCount = 0;	//bigger than threshold
	int rightCount = 0;	//lower than threshold
	int leftPosCount = 0;
	int leftNegCount = 0;
	int rightPosCount = 0;
	int rightNegCount = 0;

	float compareVal = 0.0f;
	//std::cout << "getGain Debug DataIndListSize is" << DataIndListSize << std::endl;
	//std::cout << "getGain Debug threshold is" << threshold << std::endl;
	for (int i = 0; i < DataIndListSize; i++)
	{
		if (Index == 0)
		{
			compareVal = TrainData[DataIndList[i]].x;
		}
		else
		{
			compareVal = TrainData[DataIndList[i]].y;
		}
		if (compareVal > threshold)
		{
			leftCount++;
			if (TrainData[DataIndList[i]].label == POS_CLASS)
			{
				leftPosCount++;
			}
			else
			{
				leftNegCount++;
			}
		}
		else
		{
			rightCount++;
			if (TrainData[DataIndList[i]].label == NEG_CLASS)
			{
				rightPosCount++;
			}
			else
			{
				rightNegCount++;
			}
		}

	}

	//printf("%d, %d, %d\n", leftCount, leftPosCount, leftNegCount);
	//printf("%d, %d, %d\n", rightCount, rightPosCount, rightNegCount);

	float leftEntropy = getEntropy(leftPosCount, leftNegCount);
	float rightEntropy = getEntropy(rightPosCount, rightNegCount);

	int parentPosCount = leftPosCount + rightPosCount;
	int parentNegCount = leftNegCount + rightNegCount;
	float parentEntropy = getEntropy(parentPosCount, parentNegCount);

	//float parentCount = parentPosCount + parentNegCount;
	//float parentPosProb = (parentPosCount + eps) / (parentCount + eps);
	//float parentNegProb = (parentNegCount + eps) / (parentCount + eps);
	//float parentEntropy = -1.0f * (parentPosProb*log(parentPosProb + eps) 
	//					+ parentNegProb*log(parentNegProb + eps));

	Gain = parentEntropy - (leftEntropy * leftCount/(leftCount+rightCount))
			 			 - (rightEntropy * rightCount/(leftCount+rightCount));
	return Gain;
}

float Tree::getEntropy(int PosCount, int NegCount)
{
	int Count = PosCount + NegCount;
	float eps = 0.0000001f;	//ToDo move to init
	float PosProb = (PosCount + eps) / (Count + eps);
	float NegProb = (NegCount + eps) / (Count + eps);
	float Entropy = -1.0f * (PosProb*log(PosProb + eps) 
						+ NegProb*log(NegProb + eps));
	return Entropy;
}

void Tree::getProb(Node* _node, Data* TrainData)
{
	int PosCount = 0;
	int NegCount = 0;
	for (int i = 0; i < _node->DataIndListSize; i++)
	{
		if (TrainData[_node->DataIndList[i]].label == 0)
		{
			PosCount++;	
		}
		else
		{
			NegCount++;
		}
	}
	int Count = PosCount + NegCount;
	float eps = 0.0000001f;	//ToDo move to init
	_node->PosProb = (PosCount + eps) / (Count + eps);
	_node->NegProb = (NegCount + eps) / (Count + eps);

	return;
}




void Tree::decideNodeThreshold(Node* node, Data* TrainData)
{
	//current node data check
	node->isLeafFlag = isLeafNode(node->DataIndList, TrainData, node->DataIndListSize);
	printf("curent node is Leaf Node = %d\n", node->isLeafFlag);
	printf("List in decide node threshold");
	for (int i = 0; i < node->DataIndListSize; i++)
	{
		printf("%d, ", node->DataIndList[i]);
	}

	if (node->isLeafFlag)
	{
		printf("current node is LeafNode.");
		node->threshold = (float)NULL;
    	node->FeatureInd = (int)NULL;	// x(=0) or y(=1)
    	getProb(node, TrainData);
		return;
	}

	float threshold = 0.0f;
    float Gain = 0.0f;
    float maxGain = 0.0f;
    int Index = 0;
    std::cout << "List size Debug: " << node->DataIndListSize << std::endl;
    Index = rand()%2;
    std::cout << "Feature Index Debug: " << Index << std::endl;
    for (int isample = 0; isample < node->DataIndListSize; isample++)
    {
    	if (Index == 0)
    	{
    	    threshold = TrainData[node->DataIndList[isample]].x;
    	}
    	else
    	{
    	    threshold = TrainData[node->DataIndList[isample]].y;	
    	}

    	Gain = getGain(threshold, TrainData, Index, node->DataIndList, node->DataIndListSize);
    	//std::cout << "Threshold:" << threshold << ", Gain:" << Gain << std::endl;

    	if(maxGain < Gain)
    	{
    		maxGain = Gain;
    		node->threshold = threshold;
    		node->FeatureInd = Index;	// x(=0) or y(=1)
    	}
    	else
    	{
    		//do nothing
    	}
    }
	std::cout << "selected Threshold:" << node->threshold << ", Gain:" << maxGain << ", FeatureIndex:" << Index << std::endl;

	return;
}

//void Tree::separate(int* leftDataIndList, int* rightDataIndList, Node node, int* DataIndList, Data* TrainData, int* leftDataNum, int* rightDataNum)
void Tree::separate(Node* NodeList, Data* TrainData, int Depth, int Width, int TargetInd)
{
	int iLeftList = 0;
	int iRightList = 0;
	float compareVal = 0.0f;
	calcChildNodeIndex(Depth, Width, &(NodeList[TargetInd]), TargetInd);

	Node _LeftChildNode = NodeList[NodeList[TargetInd].LeftChildNode];
	Node _RightChildNode = NodeList[NodeList[TargetInd].RightChildNode];

	int LeftChildNodeInd = NodeList[TargetInd].LeftChildNode;
	int RightChildNodeInd = NodeList[TargetInd].RightChildNode;

	if (NodeList[TargetInd].DataIndListSize != 0 || !NodeList[TargetInd].isLeafFlag)
	{
		//separate and count using threshold
		for (int i = 0; i < NodeList[TargetInd].DataIndListSize; i++)
		{
			//Index Select
			if (NodeList[TargetInd].FeatureInd == 0)
			{
				compareVal = TrainData[NodeList[TargetInd].DataIndList[i]].x;
			}
			else
			{
				compareVal = TrainData[NodeList[TargetInd].DataIndList[i]].y;
			}

			//compare
			if (compareVal > NodeList[TargetInd].threshold)
			{
				_LeftChildNode.DataIndList[iLeftList] = NodeList[TargetInd].DataIndList[i];

				//leftDataIndList[iLeftList] = DataIndList[i];
				iLeftList++;
			}
			else
			{
				_RightChildNode.DataIndList[iRightList] = NodeList[TargetInd].DataIndList[i];
				//rightDataIndList[iRightList] = DataIndList[i];
				iRightList++;
			}

		}
		if (Depth != TREE_DEPTH - 1)
		{
			_LeftChildNode.DataIndListSize = iLeftList;
			_RightChildNode.DataIndListSize = iRightList;


			NodeList[LeftChildNodeInd] = _LeftChildNode;
			NodeList[RightChildNodeInd] = _RightChildNode;

			//NodeList[LeftChildNodeInd].isLeafFlag = isLeafNode(NodeList[LeftChildNodeInd].DataIndList, TrainData, NodeList[LeftChildNodeInd].DataIndListSize);
			//NodeList[RightChildNodeInd].isLeafFlag = isLeafNode(NodeList[RightChildNodeInd].DataIndList, TrainData, NodeList[RightChildNodeInd].DataIndListSize);

			//debug
			printf("List in separate\n");
			printf("LeftList:");
			for (int i = 0; i < iLeftList; i++)
			{
				printf(" %d, ", NodeList[LeftChildNodeInd].DataIndList[i]);
			}
			printf("\n");
			printf("RightList:");
			for (int i = 0; i < iRightList; i++)
			{
				printf("%d, ", NodeList[RightChildNodeInd].DataIndList[i]);
			}
			printf("\n");
			//printf("LeftNode isLeafFlag %d, LeftChildNode = %d\n", NodeList[LeftChildNodeInd].isLeafFlag, LeftChildNodeInd);
			//printf("RightNode isLeafFlag %d RightChildNode = %d\n", NodeList[RightChildNodeInd].isLeafFlag, RightChildNodeInd);
		}
		else
		{
			printf("Depth Limit \n");
			NodeList[TargetInd].isLeafFlag = true;
			for (int i = 0; i < NodeList[TargetInd].DataIndListSize; i++)
			{
				printf("%d, ", NodeList[TargetInd].DataIndList[i]);
			}
		}
	}
	else
	{
		NodeList[LeftChildNodeInd].isLeafFlag = true;
		NodeList[LeftChildNodeInd].DataIndListSize = 0;
		
		NodeList[RightChildNodeInd].isLeafFlag = true;
		NodeList[RightChildNodeInd].DataIndListSize = 0;
	}

	return;
}

void Tree::calcChildNodeIndex(int iDepth, int iWidth, Node* node, int TargetInd)
{
	//int nodeInd = (2^iDepth) + iWidth - 1;
	node->LeftChildNode = 2*TargetInd + 1;
	node->RightChildNode = 2*TargetInd + 2;

	return;
}


bool Tree::isLeafNode(int* DataIndList, Data* TrainData, int DataNum)
{
	int iLeftList = 0;
	float compareVal = 0.0f;
	int PosCount = 0;
	int NegCount = 0;
	for (int i = 0; i < DataNum; i++)
	{
		if (TrainData[DataIndList[i]].label == 0)
		{
			PosCount++;	
		}
		else
		{
			NegCount++;
		}
	}
	float checkEntropy = getEntropy(PosCount, NegCount);
	if ((PosCount == 0 && NegCount != 0) || (PosCount != 0 && NegCount == 0))
	{
		checkEntropy = 0.0f;
	}

	printf("Debug Entropy is %f\n", checkEntropy);
	if (checkEntropy < 0.0001f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Tree::predict(Node* NodeList, Data TestData)
{
	int Index = 0;

	int TargetInd = 0;
	while(!NodeList[TargetInd].isLeafFlag)
	{
		Index = NodeList[TargetInd].FeatureInd;
		if (Index == 0)
		{
			//compare in x
			if (TestData.x > NodeList[TargetInd].threshold)
			{
				//go to left
				TargetInd = NodeList[TargetInd].LeftChildNode;
			} 
			else
			{
				//go to right
				TargetInd = NodeList[TargetInd].RightChildNode;	
			}
		}
		else
		{
			//compare in y
			if (TestData.y > NodeList[TargetInd].threshold)
			{
				//go to left
				TargetInd = NodeList[TargetInd].LeftChildNode;
			} 
			else
			{
				//go to right
				TargetInd = NodeList[TargetInd].RightChildNode;	
			}
		}
	}
	return TargetInd;
}

int main(int argc, char** argv)
{

	//Make Dataset
	Data TrainData[DATA_NUM];
	Data TestData[DATA_NUM];

	//random seed
	srand(10);

	//Make Data
	printf("------------TrainData-----------\n");
    for(int i=0; i<DATA_NUM; i++)
    {
    	if (i < DATA_NUM/2)
    	{
    		TrainData[i].x = (float)(rand()%256) /(256.0f * 4.0f) + 0.2f;
			TrainData[i].y = (float)(rand()%256) /(256.0f * 4.0f) + 0.2f;
			TrainData[i].label = POS_CLASS;
    	}
    	else
    	{
    		TrainData[i].x = (float)(rand()%256) /(256.0f * 4.0f) + 0.4f;
			TrainData[i].y = (float)(rand()%256) /(256.0f * 4.0f) + 0.4f;
			TrainData[i].label= NEG_CLASS;
    	}
    	printf("%d, %f, %f\n", TrainData[i].label, TrainData[i].x,TrainData[i].y);
  	}
  	printf("------------TestData-----------\n");
  	for(int i=0; i<DATA_NUM; i++)
    {
    	if (i < DATA_NUM/2)
    	{
    		TestData[i].x = (float)(rand()%256) /(256.0f * 4.0f) + 0.2f;
			TestData[i].y = (float)(rand()%256) /(256.0f * 4.0f) + 0.2f;
			TestData[i].label = POS_CLASS;
    	}
    	else
    	{
    		TestData[i].x = (float)(rand()%256) /(256.0f * 4.0f) + 0.4f;
			TestData[i].y = (float)(rand()%256) /(256.0f * 4.0f) + 0.4f;
			TestData[i].label= NEG_CLASS;
    	}
    	printf("%d, %f, %f\n", TestData[i].label, TestData[i].x,TestData[i].y);
  	}
    getchar();

    //Make decision tree
    Tree SingleTree;
    Tree Forest[SUB_SET];

    for (int iSubset = 0; iSubset < SUB_SET; iSubset++)
    {
    	//init
	    for (int iDepth = 0; iDepth < TREE_DEPTH; iDepth++)
	    {
	    	for (int iWidth = 0; iWidth < pow(2,iDepth); iWidth++)
	    	{
	    		int TargetInd = pow(2, iDepth) + iWidth - 1;
				SingleTree.NodeList[TargetInd].DataIndListSize = 0; //init
				SingleTree.NodeList[TargetInd].isLeafFlag = false; 	//init
				SingleTree.NodeList[TargetInd].PosProb = 0.0f; 		//init
				SingleTree.NodeList[TargetInd].NegProb = 0.0f; 		//init
			}
		}

		//Make decision tree
		for (int iDepth = 0; iDepth < TREE_DEPTH; iDepth++)
    	{
    		for (int iWidth = 0; iWidth < pow(2,iDepth); iWidth++)
    		{
	    		int TargetInd = pow(2, iDepth) + iWidth - 1;
	    		int ParentInd = 0;
	    		//calc parent node ind
	    		if (iDepth > 0)
	    		{
	    			if (TargetInd%2 == 0)
					{
						ParentInd = (int)(max(TargetInd - 2, 0) / 2);
					}
					else
					{
						ParentInd = (int)(max(TargetInd - 1, 0) / 2);	
					}
	    		}
	    		else
	    		{
	    			//do nothing
	    		}
    			std::cout << "Depth:" << iDepth << ", Width:" << iWidth << ", TargetInd:" << TargetInd <<std::endl;

	    		if (!SingleTree.NodeList[ParentInd].isLeafFlag && !SingleTree.NodeList[TargetInd].isLeafFlag)
	    		{
			    	if (iDepth == 0 && iWidth == 0)
		    		{
		    			//Make subset data (root node)
		    			for (int isample = 0; isample < SUB_SET_SAMPLE; isample++)	
		    			{
		    				SingleTree.NodeList[0].DataIndList[isample] = (rand()%DATA_NUM);
		    			}
		    			SingleTree.NodeList[0].DataIndListSize = SUB_SET_SAMPLE;

		    			//debug
		    			for (int isample = 0; isample < SUB_SET_SAMPLE; isample++)
		    			{
		    				printf("%d, %d, %f, %f\n", 
		    						TrainData[SingleTree.NodeList[0].DataIndList[isample]].label, 
		    						SingleTree.NodeList[0].DataIndList[isample], 
		    						TrainData[SingleTree.NodeList[0].DataIndList[isample]].x, 
		    						TrainData[SingleTree.NodeList[0].DataIndList[isample]].y);
		    			}
		    		}
		    		std::cout << "Node Index" << TargetInd << std::endl;
		    		SingleTree.decideNodeThreshold(&(SingleTree.NodeList[TargetInd]), TrainData);
		    		//std::cout << "threshold and index: " << SingleTree.NodeList[TargetInd].threshold << ", " << SingleTree.NodeList[TargetInd].FeatureInd << std::endl;
		    		if (!SingleTree.NodeList[TargetInd].isLeafFlag)
		    		{
		    			SingleTree.separate(SingleTree.NodeList, TrainData, iDepth, iWidth, TargetInd);
		    		}
		    		else
		    		{
		    			//do nothing
		    			//Target Node is already leafnode. 

		    		}
	    		}
	    		else
	    		{
	    			SingleTree.NodeList[TargetInd].isLeafFlag = true;
	    			std::cout << "Depth:" << iDepth << ", Width:" << iWidth << ", TargetInd:" << TargetInd << " is LeafNode." << SingleTree.NodeList[TargetInd].DataIndListSize <<std::endl;
	    		}
    		}
	    }
	    std::cout << "-----------check tree work result-------------" << std::endl;
	    for (int iDepth = 0; iDepth < TREE_DEPTH; iDepth++)
	    {
	    	for (int iWidth = 0; iWidth < pow(2,iDepth); iWidth++)
	    	{
	    		//std::cout << "Depth:" << iDepth << ", Width:" << iWidth << ", TargetInd:" << TargetInd <<std::endl;
	    		int TargetInd = pow(2, iDepth) + iWidth - 1;
	    		if (!SingleTree.NodeList[TargetInd].isLeafFlag)
	    		{
	    			//do nothing
	    		}
	    		else
	    		{
	    			std::cout << "Depth:" << iDepth << ", Width:" << iWidth << ", TargetInd:" << TargetInd << " is LeafNode. " << SingleTree.NodeList[TargetInd].DataIndListSize <<std::endl;

	    			for (int isample = 0; isample < SingleTree.NodeList[TargetInd].DataIndListSize; isample++)
	    			{
	    				int Ind = SingleTree.NodeList[TargetInd].DataIndList[isample];
	    				printf("%d, %d, %f, %f\n", 
		    						TrainData[SingleTree.NodeList[TargetInd].DataIndList[isample]].label, 
		    						SingleTree.NodeList[TargetInd].DataIndList[isample], 
		    						TrainData[SingleTree.NodeList[TargetInd].DataIndList[isample]].x, 
		    						TrainData[SingleTree.NodeList[TargetInd].DataIndList[isample]].y);
	    			}
	    		}
	    	}
	    }
	    std::cout << "-----------done-------------" << std::endl;
	    //copy SingleTree to Forest
	    Forest[iSubset] = SingleTree;
    }

    //predict
    for (int iData = 0; iData < DATA_NUM; iData++)
    {
    	float sumPosProb = 0.0f;
    	float sumNegProb = 0.0f;
    	for (int iSubset = 0; iSubset < SUB_SET; iSubset++)
    	{
    		int LeafNodeInd = Forest[iSubset].predict(Forest[iSubset].NodeList, TestData[iData]);
    		sumPosProb += Forest[iSubset].NodeList[LeafNodeInd].PosProb;
    		sumNegProb += Forest[iSubset].NodeList[LeafNodeInd].NegProb;
    	}
    	if (sumPosProb > sumNegProb)
    	{
    		printf("TestData[%d] label is %d\n", iData, POS_CLASS);
    	}
    	else
    	{
    		printf("TestData[%d] label is %d\n", iData, NEG_CLASS);	
    	}
    }

	return 0;
}
